# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char { |ch| str.delete!(ch) if ch == ch.downcase }
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    return str[(str.length - 1)/2]
  else
    return str[(str.length/2 - 1)..str.length/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count{ |ch| VOWELS.include?(ch.downcase)}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_str = ""
  arr.each_with_index do |el, index|
    if index != arr.length - 1
      new_str += el.to_s + separator
    else
      new_str += el.to_s
    end
  end
  new_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = ""
  str.chars.each_with_index do |ch, index|
    if (index + 1).odd?
      new_str[index] = ch.downcase
    else
      new_str[index] = ch.upcase
    end
  end
  new_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split

  words.map! do |word|
    if word.length >= 5
      word.reverse
    else
      word
    end
  end

  words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = (1..n).to_a
  array.map! do |num|
    if num % 5 == 0 && num % 3 == 0
      "fizzbuzz"
    elsif num % 5 == 0
      "buzz"
    elsif num % 3 == 0
      "fizz"
    else
      num
    end
  end
  array

end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed_array = (1..arr.length).to_a
  reversed_array.map! do |el|
    arr[-el]
  end
  reversed_array

end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  counter = 1
  num_of_primes = 0
  while counter < num + 1
    num_of_primes += 1 if num % counter == 0
    counter += 1
  end

  num_of_primes == 2

end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factor_array = []
  (1..num).to_a.each do |int|
    factor_array << int if num % int == 0
  end
  factor_array.sort!
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |fact| prime?(fact)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |int|
    if int.even?
      evens << int
    else
      odds << int
    end
  end

  if evens.count > odds.count
    return odds[0]
  else
    return evens[0]
  end
end
